import 'dart:async';

import 'package:flutter_app/bloc/counter_event.dart';

class CounterBloc {
  int _counter = 0;

  final _counterStateController = StreamController<int>();

  // privte
  StreamSink<int> get _inCounter => _counterStateController.sink;

  //public, for the state
  Stream<int> get counter => _counterStateController.stream;

// to connect the event and state inside the tube (Bloc)
  final _counterEventController = StreamController<CounterEvent>();

  Sink<CounterEvent> get counterEventSink => _counterEventController.sink;

  CounterBloc() {
    // Whenever there is a new event, we want to map it to a new state
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(CounterEvent event) {
    if (event is IncrementEvent) {
      _counter++;
    } else {
      _counter--;
    }
    _inCounter.add(_counter);
  }

  //add Dispose function to aviod memory leak

  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}
