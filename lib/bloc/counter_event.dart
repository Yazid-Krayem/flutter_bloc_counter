
// base class for Increment and Decrement
abstract class CounterEvent {}

// IncrementEvent class
class IncrementEvent extends CounterEvent {}

// DecrementEvent class
class DecrementEvent extends CounterEvent {}

